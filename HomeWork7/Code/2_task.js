/* Реализовать функцию для создания объекта "пользователь".
		Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
		При вызове функция должна спросить у вызывающего имя и фамилию.
		Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
		Добавить в объект newUser метод getLogin(), который будет возвращать 
		первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем 
		регистре (например, Ivan Kravchenko → ikravchenko).
		Создать пользователя с помощью функции createNewUser(). 
		Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.


		...
		Дополнить функцию createNewUser() методами подсчета возраста пользователя и его паролем.
		Возьмите выполненное задание выше (созданная вами функция createNewUser()) и дополните ее следующим функционалом:
		При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
		Создать метод getAge() который будет возвращать сколько пользователю лет.
		Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, 
		соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

		Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
		*/



		function CreateNewUser() {
			let name1 = prompt("Введи своє ім'я...");
			let surName = prompt("Введи своє прізвище...");
			let birthday = prompt("Введи дату народження...", "dd.mm.yyyy");

			this.newUser = {
				firstName: name1,
				lastName: surName,

				getLogin() {
					return name1[0].toLowerCase() + surName.toLowerCase();
				},
				
				getAge() {
					return "Birth date: " + birthday;
				},

				getPassword() {
					return name1[0].toUpperCase() + surName.toLowerCase() + birthday[6] + birthday[7] + birthday[8] + birthday[9];
				},
			};
		}

		const createNewUser = new CreateNewUser();
		console.log("Result getLogin(): " + createNewUser.newUser.getLogin());
		console.log("Result getAge(): " + createNewUser.newUser.getAge());
		console.log("Result getPassword(): " + createNewUser.newUser.getPassword());