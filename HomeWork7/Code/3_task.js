		/*
		Реализовать функцию фильтра массива по указанному типу данных.
		Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. 
		Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
		Функция должна вернуть новый массив, который будет содержать в себе все данные, 
		которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. 
		То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', 
		то функция вернет массив [23, null].
		*/

		let testArray = ['hello', 'world', 23, '23', null]; // Перший аргумент
		// bool, number, string - Другий аргумент

		function FilterBy(arr, check) {
			console.info("Заданий масив:");
			console.dir(arr);
			console.info("Задана перевірка на:");
			console.log(check);
			console.info("Результат:");

			let newArr = arr.filter(item => typeof item !== check);
			return newArr
		}

		let filter = new FilterBy(testArray, "string");
		console.log(filter);