/* Сделайте функцию, каждый вызов который будет генерировать случайные числа от 1 до 100, 
но так, чтобы они не повторялись, пока не будут перебраны все числа из этого промежутка. 
Решите задачу через замыкания - в замыкании должен хранится массив чисел, 
которые уже были сгенерированы функцией. */

let button = document.querySelector("button");

button.addEventListener("click", function() {
	console.log(x());
});

function randomNumber(num) {
	let usedNumbers = [];
	
	function f() {
		if(usedNumbers.length == num) {
			console.log(usedNumbers);
			console.log("Використалися всі випадкові числа.");
			usedNumbers = [];
		};

		let i = Math.ceil(Math.random() * num);

		if(usedNumbers.includes(i)) {
			return f();
		} else {
			usedNumbers.push(i);
			return i;
		};
	};

	return f;
	
}; 

let x = randomNumber(100);


