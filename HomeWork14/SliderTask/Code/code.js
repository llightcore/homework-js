// Кнопка тікає від користувача
const banner = document.getElementById("banner");
const priceButton = banner.querySelector("button");

const windowWidth = window.innerWidth; // Ширина вікна сторінки
const windowHeight = window.innerHeight; // Висота вікна сторінки
const buttonWidth = banner.clientWidth; // Ширина елементу без margin
const buttonHeight = banner.clientHeight; // Висота елементу без margin

priceButton.addEventListener("mouseover", () => {
	let x = Math.floor(Math.random() * (windowWidth - buttonWidth));
	let y = Math.floor(Math.random() * (windowHeight - buttonHeight));
	banner.style.right = x + "px";
	banner.style.bottom = y + "px";
}); 

// Перевірка введених даних та реалізація кнопок
const btnSubmit = document.querySelector("[value = 'Підтвердити замовлення >>']");
const btnReset = document.querySelector("[type = 'reset']");
const [...inputs] = document.querySelectorAll("[placeholder]");

let isNameValid, isNumValid, isMailValid;

function nameValid(inputs) {
	isNameValid = /^[а,i-ї-я]{2,}$|^[a-z]{2,}$/i.test(inputs.value);
	inputs.value = inputs.value.charAt(0).toUpperCase() + inputs.value.slice(1);

	if(!isNameValid) {
		inputs.classList.remove("success");
		inputs.classList.add("error");
	} else {
		inputs.classList.remove("error");
		inputs.classList.add("success");
	};
};
function numValid(inputs) {
	isNumValid = /^\+380\d{9}$/.test(inputs.value);
	if(!isNumValid) {
		inputs.classList.remove("success");
		inputs.classList.add("error");
	} else {
		inputs.classList.remove("error");
		inputs.classList.add("success");
	};
};
function mailValid(inputs) {
	isMailValid = /^[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,}$/.test(inputs.value);
	if(!isMailValid) {
		inputs.classList.remove("success");
		inputs.classList.add("error");
	} else {
		inputs.classList.remove("error");
		inputs.classList.add("success");
	};
};

inputs.forEach(inputs => {
	inputs.addEventListener("input", () => {
		switch(inputs.name){
			case "name": nameValid(inputs); break;
			case "phone": numValid(inputs); break;
			case "email": mailValid(inputs); break;
		};
	});
});

// Кнопка підтвердити
btnSubmit.addEventListener("click", () => {
	if((isNameValid & isNumValid & isMailValid) == true){ // Якщо в масиві немає false
		console.log("Success");
		window.location.href = "thank-you.html"; // Перехід на кінцеву сторінку
	};
});

// Кнопка скинути
btnReset.onclick = () => {
	inputs.forEach(inputs => {
		inputs.value = "";
		inputs.classList.remove("error");
		inputs.classList.remove("success");
	});
};

	//!
	let saucesPrice = null; // 30
	let topingsPrice = null; // 20

// Перетягування
const tablePizza = document.querySelector(".table");
const saucesTable = document.querySelector(".saucesPrice");
const saucesTableContent = saucesTable.querySelector("p");
const topingsTable = document.querySelector(".topingsPrice");
const topingsTableContent = topingsTable.querySelector("p");

let draggable = document.querySelectorAll(".draggable");
let cloneDragAdd = null;

draggable.forEach(element => {
	element.addEventListener("dragstart", function(e) {
		setTimeout(() => {
			this.style.visibility = "hidden";
		},0);
		let dragAdd = e.target;
		cloneDragAdd = dragAdd.cloneNode(true); // ? Копія елемента
	});
	element.addEventListener("dragend", function() {
		this.style.visibility = "visible";
	});
});

tablePizza.addEventListener("dragover", function(e) {
	e.preventDefault();
});
tablePizza.addEventListener("dragenter", function(e) {
	e.preventDefault();
	this.classList.add("bBorder");
});
tablePizza.addEventListener("dragleave", function() {
	this.classList.remove("bBorder");
});
tablePizza.addEventListener("drop", function() {                // TODO Drop

	let sauceClass = tablePizza.querySelector("img.sauces");
	let topingsClass = tablePizza.querySelectorAll("img.topings");
	let clonDragSauces = cloneDragAdd.classList.contains("sauces");
	let clonDragTopings = cloneDragAdd.classList.contains("topings");

	if(!sauceClass && clonDragTopings) {
		this.classList.remove("bBorder");
		return;
	} else if(sauceClass && clonDragSauces) sauceClass.remove();
	if(sauceClass && topingsClass && clonDragSauces) {
		sauceClass.remove();
		let korj = tablePizza.children[1];
		this.insertBefore(cloneDragAdd, korj);
		this.classList.remove("bBorder");
		return;
	} else if (topingsClass.length >= 3) {
		this.classList.remove("bBorder");
		return;
	};

	//? input
	if(clonDragSauces) {
		this.append(cloneDragAdd);
		saucesPrice = 30;
		saucesTableContent.innerHTML = `Соуси: ${saucesPrice}UAH`;
		parseInfo = parseInt(priceTextContent.innerHTML);
		parseLoad = parseInfo + saucesPrice;
		priceTextContent.innerHTML = `${parseLoad}UAH`;
	};
	if(clonDragTopings) {
		this.append(cloneDragAdd);
		topingsPrice += 20;
		topingsTableContent.innerHTML = `Топінги: ${topingsPrice}UAH`;
		let parseInfo = parseInt(priceTextContent.innerHTML);
		let parseLoad = parseInfo + 20;
		priceTextContent.innerHTML = `${parseLoad}UAH`;
	};
	this.classList.remove("bBorder");
});

// Відображення ціни при зміні радіо-кнопок
const priceTable = document.querySelector(".price");
const priceTextContent = priceTable.querySelector("p");

const [...radioButtons] = document.getElementsByClassName("radioIn");
let radioChecked = "";

radioButtons.forEach(element => {
	if(element.checked) radioChecked = element.id; 
	
	switch(radioChecked){
		case "small": priceTextContent.innerHTML = `${90}UAH`; break;
		case "mid": priceTextContent.innerHTML = `${150}UAH`; break;
		case "big": priceTextContent.innerHTML = `${220}UAH`; break;
	};

	element.addEventListener("change", (event) => {
		switch(event.target.id){
			case "small": {
				priceTextContent.innerHTML = `${90 + saucesPrice + topingsPrice}UAH`;
				break;
			};
			case "mid": {
				priceTextContent.textContent = `${150 + saucesPrice + topingsPrice}UAH`; 
				break;
			};
			case "big": {
				priceTextContent.textContent = `${220 + saucesPrice + topingsPrice}UAH`; 
				break;
			};
		};
	});
});