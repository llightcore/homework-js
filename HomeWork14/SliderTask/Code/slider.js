$(document).ready(function() {
	let offset = 0;
	const sliderLine = $(".slider-line");

	function btnNext() {
		offset = offset + 900;
		if(offset > 1800) {
			offset = 0
		}
		sliderLine.css("left", -offset + "px");
	};

	setInterval(btnNext, 15000);
	$(".button-next").click(btnNext);

	$(".button-prev").click(function() {
		offset = offset - 900;
		if(offset < 0) {
			offset = 1800
		}
		sliderLine.css("left", -offset + "px");
	});
});
