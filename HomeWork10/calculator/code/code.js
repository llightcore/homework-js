let a = "";
let b = "";
let operator = "";
let memory = "";
let bul = true;

const display = document.getElementById("display");

const keys = document.querySelector(".keys");

const numbers = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."];
const actions = ["-", "+", "*", "/"];
const functions = ["mrc", "m-", "m+"];

// Для клавіші C
function clearAll () {
	a = "";
	b = "";
	operator = "";
	display.value = "";
};

// Розрахунок
function calc() {
	switch(operator) {
		case "+":{
			a = (+a) + (+b);
			break;
		}
		case "-":{
			a = a - b;
			break;
		}
		case "*":{
			a = a * b;
			break;
		}
		case "/":{
			a = a / b;
			break;
		}
	};
	display.value = a;
	operator = "";
	b = "";
	a = a + "";
};

keys.addEventListener("click", function(e) {
	if (e.target.closest(".button")) {
		console.log(e.target);
		let key = e.target.value;
		display.style.textAlign = "right"; 

		if(numbers.includes(key)){
			if(operator == "") {
				if(a == "" && key == ".") return;
				if(a.includes(".") && key == ".") return;
				if(a.length < 18) a += key;
				display.value = a;
				console.log("Перше значення: " + a);
			};
			
			if(a !== "" && operator !== ""){
				if(b == "" && key == ".") return;
				if(b.includes(".") && key == ".") return;
				display.value = "";
				if(b.length < 18) b += key;
				display.value = b;
				console.log("Друге значення: " + b);
			};
		};

		if(actions.includes(key)){
			if(a == "") return;
			if(a !== "" && b !== "" && operator !== "") calc();

			operator = key;
			console.log("Оператор: " + operator);
		};

		// m+
		if(key == "m+") {
			if(display.value == "" || display.value == "m" || display.value == "del") {
				display.style.textAlign = "left";
				return;
			};
			display.style.textAlign = "left";
			memory = display.value;
			display.value = "m";
			a = ""; b = ""; operator = "";

			console.table("Memory: " + memory);	
		};

		// m-
		 if(key == "m-") {
			if(memory !==  "") {
				display.style.textAlign = "left"
				memory = "";
				display.value = "del";
			} else if(display.value == "del") display.style.textAlign = "left";
		}; 

		// mrc
		if(key == "mrc") {
			if(display.value == "del") display.style.textAlign = "left";
			if(memory == "") return;
			display.value = memory;

			if(bul == true) {
				if(b == "" && operator == "") {
					a = memory;
					console.log("перше тепер " + a);
				} else if(a !== "" && operator !== "") {
					b = memory;
					console.log("друге тепер " + b);
				};
				bul = false;
			} else {
				display.style.textAlign = "left"
				memory = "";
				display.value = "del";
				bul = true;
			}; 
		};
	
		
		if(key == "=") calc();

		if(display.value.length >= 18) {
			display.value = display.value.slice(0,18);
		};

		if(key == "C") clearAll();
	};
});