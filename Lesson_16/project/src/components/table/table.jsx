import React from "react";

function Table (props){
	return (
		<table>
			<tbody>
				{props.data.map((el) => {
					return <>
						<h3>txt: {el.txt}</h3>
						<p>r030: {el.r030}</p>
						<p>rate: {el.rate}</p>
					</>
				})}
			</tbody>
		</table>
	)
}

export default Table;