var author = "llightcore";
console.log(author);

var x = 6;
var y = 14;
var z = 4;

var result1;

result1 = x += y - x++ * z;
/* 
	1 - (x++) 
	2 - (*)  
	3 - (-)  
	4 - (+=) 
*/
 
document.writeln(result1);



var x = 6;
var y = 14;
var z = 4;

var result2;

result2 = z = --x - y * 5;
/* 
	1 - (--x)  
	2 - (*) 
	3 - (-) 
	4 - (=) 
*/

document.writeln(result2);



var x = 6;
var y = 14;
var z = 4;

var result3;

result3 = y /= x + 5 % z;
/* 
	1 - (%)  
	2 - (+)  
	3 - (/=) 
*/

document.writeln(result3);



var x = 6;
var y = 14;
var z = 4;

var result4;

result4 = z - x++ + y * 5;
/* 
	1 - (x++) 
	2 - (*)  
	3 - (-)  
	4 - (+)  
*/

document.writeln(result4);



var x = 6;
var y = 14;
var z = 4;

var result5;

result5 = x = y - x++ * z;
/* 
	1 - (x++) 
	2 - (*)  
	3 - (-)  
	4 - (=) 
*/

document.writeln(result5);

