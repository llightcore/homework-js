/*
Создайте функцию-конструктор Calculator, который создаёт объекты с тремя методами:
	read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
	sum() возвращает сумму введённых свойств.
	mul() возвращает произведение введённых свойств.
*/

function Calculator() {
	let x;
	let y;

	this.read = function () {
		x = Number(prompt("Введи перше значення:"));
		y = Number(prompt("Введи друге значення:"));
		return;
	};
	this.sum = function () {
		document.write("Результат додавання: " + Number(x + y) + "<br>");
	};
	this.mul = function () {
		document.write("Результат множення: " + x * y);
	};
};

let result = new Calculator();

result.read();
result.sum();
result.mul();