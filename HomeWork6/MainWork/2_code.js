/* 
Разработайте функцию-конструктор, которая будет создавать объект Human (человек), 
добавьте на свое усмотрение свойства и методы в этот объект. 
Подумайте, какие методы и свойства следует сделать уровня экземпляра, а какие уровня функции-конструктора
*/

function Human(name, surname, age) {
	this.name = name;
	this.surname = surname;
	this.age = age;

	this.showInfo = function() {
		document.write(`Вітаю на зборі!. Мене звати ${this.name}. 
		По прізвищу можна мене кликати ${this.surname}. Мені ${this.age} років<p>`);
	}
}

let user1 = new Human("Andrii", "Smirnow", "15");
let user2 = new Human("Max", "Shevchenko", "20");
let user3 = new Human("Tom", "Podolyak", "31");
let user4 = new Human("Omar", "Karpa", "42");

user1.showInfo();
user2.showInfo();
user3.showInfo();
user4.showInfo();



 


 