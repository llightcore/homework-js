// Завдання 2
let task2 = document.createElement("h4");
task2.textContent = "Завдання 2";
document.body.prepend(task2);

let a = document.getElementById('a');
let b = document.getElementById('b');

function replace() {
	[a.value, b.value] = [b.value, a.value];
}


// Завдання 3
let [...divs] = document.getElementsByTagName("div");

divs.forEach((item, index) => {
	index++;
	let para = document.createElement("p");
	para.textContent = index;
	item.append(para);

	item.style.backgroundColor = "aqua";
});

 
// Завдання 4
let inputV = document.getElementById("gen");
let paste = document.getElementById("paste");

let divTag = document.createElement("div");

function afterClick() {
	divTag.innerText = inputV.value;
	paste.append(divTag);
	inputV.value = "";
}


// Завдання 5
let image = document.getElementById("image");
let buttonC = document.getElementById("buttonC");

image.setAttribute("src", "https://itproger.com/img/courses/1476977240.jpg");
image.setAttribute("alt", "itproger");

let count = 0;
function ifClick() {
	if (count == 0) {
		image.src = "https://itproger.com/img/courses/1476977240.jpg";
		count++;
	} else if(count == 1) {
		image.src = "https://itproger.com/img/courses/1476977488.jpg";
		count++;
	} else if(count == 2) {
		image.src = "https://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Unofficial_JavaScript_logo_2.svg/1200px-Unofficial_JavaScript_logo_2.svg.png";
		count = 0;
	}
}


// Завдання 6
let deleteP = document.getElementsByClassName("deleteP");

for(let item of deleteP) {
	item.style.textAlign = "center";
	item.style.backgroundColor = "burlywood";
	item.style.cursor = "pointer";
}

function delP(x) {
	switch(x){
		case('0'):deleteP[0].style.display = "none"; break;
		case('1'):deleteP[1].style.display = "none"; break;
		case('2'):deleteP[2].style.display = "none"; break;
		case('3'):deleteP[3].style.display = "none"; break;
		case('4'):deleteP[4].style.display = "none"; break;
		case('5'):deleteP[5].style.display = "none"; break;
		case('6'):deleteP[6].style.display = "none"; break;
		case('7'):deleteP[7].style.display = "none"; break;
		case('8'):deleteP[8].style.display = "none"; break;
		case('9'):deleteP[9].style.display = "none"; break;
	}
}



// Завдання 7
let input = document.getElementById("input");

function addElement(x) {
	input.value += x;
}
 