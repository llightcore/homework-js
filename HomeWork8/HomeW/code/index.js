let mainInput = document.querySelector("input");

let newInput = document.createElement("input");
newInput.setAttribute("type", "text");
newInput.setAttribute("placeholder", "Circle diameter");

let paintButton = document.createElement("button");
paintButton.innerText = "Намалювати!";

mainInput.onclick = function() {
	mainInput.before(newInput);
	mainInput.before(paintButton);
	mainInput.remove();
};

var circle;
var classN;

paintButton.onclick = function() {
	let value = +newInput.value;
	if (isNaN(value)) {
		newInput.value = "";
		value = undefined;
		newInput.setAttribute("placeholder" ,"Введи число!");
	} else if (value == "") {
		newInput.setAttribute("placeholder" ,"Введи хоч щось...");
	} else if (value > 100 || value < 10) {
		newInput.value = "";
		value = undefined;
		newInput.setAttribute("placeholder" ,"Інший діаметр");
	} else {
		newInput.remove();
		paintButton.remove();

		for(let i = 0; i < 100; i++){
			
			circle = document.createElement("div");
			circle.classList.add("elem_");
			circle.setAttribute("onclick", `delF(${i})`);
			 
			circle.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 45%)`;
			circle.style.width =  `${value}px`;
			circle.style.height = `${value}px`;
			circle.style.borderRadius = "50%";
			circle.style.margin = "10px";
			circle.style.cursor = "pointer";
			document.body.append(circle);
			 
			document.body.style.display = "grid";
			document.body.style.gridTemplateAreas = "'q w e r t y u i o p'";
		} 
		classN = document.getElementsByClassName("elem_");
	}
	
}

function delF(x) {
	classN[x].style.display = "none";
}



