/* - У файлі index.html лежить розмітка для кнопок
	- Кожна кнопка містить назву клівіші на клавіатурі
	- Після натискання вказаних клавіш - та кнопка, на якій написана ця 
	літера, повинна фарбуватись в синій колір. При цьому якщо якась інша  
	літера вже раніше була пофарбована у синій колір - вона стає чорною.
	Наприклад за натисканням кнопки Enter перша кнопка забарвлюється в синій колір.
	Далі, користувач натискає 'S', і кнопка 'S' забарвлюється в синій колір, а кнопка 
	'Enter' знову стає чорною. */

const [...buttons] = document.getElementsByClassName("btn");

function checkActive() {
	buttons.forEach(elements => {
		if(elements.classList.contains("activeClick")){
			elements.classList.remove("activeClick");
		};
	});
};

function checkKey(key) {
	checkActive();
	buttons.forEach(elements => {
		if(elements.textContent == key){
			elements.classList.add("activeClick");
		};
	});
};

document.addEventListener("keypress", (e) => {
	switch(e.key){
		case "Enter": return checkKey("Enter");
		case "S": return checkKey("S");
		case "s": return checkKey("S");
		case "E": return checkKey("E");
		case "e": return checkKey("E");
		case "O": return checkKey("O");
		case "o": return checkKey("O");
		case "N": return checkKey("N");
		case "n": return checkKey("N");
		case "L": return checkKey("L");
		case "l": return checkKey("L");
		case "Z": return checkKey("Z");
		case "z": return checkKey("Z");
	};
});