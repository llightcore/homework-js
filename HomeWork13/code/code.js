/*
	name
	height
	skin_color
	gender
	birth_year
	homeworld
*/

// Додавання елементів через класи
class Card {
		constructor(element, planet) {
		this.name = element.name;
		this.height = element.height;
		this.skinColor = element.skin_color;
		this.gender = element.gender;
		this.birthYear = element.birth_year;
		// Planet
		this.planet = planet.name;
	};

	createCard() {
		// ? Створення списку
		const cardDiv = document.createElement("div");
		cardDiv.classList.add("Card");
		document.body.append(cardDiv);

		const textBlockDiv = document.createElement("div");
		textBlockDiv.classList.add("text_block");
		cardDiv.append(textBlockDiv);

		const nameDiv = document.createElement("div");
		const genderDiv = document.createElement("div");
		const skinColorDiv = document.createElement("div");
		const birthYearDiv = document.createElement("div");
		const heightDiv = document.createElement("div");
		const planetDiv = document.createElement("div");

		const nameSpan = document.createElement("span");
		const genderSpan = document.createElement("span");
		const skinColorSpan = document.createElement("span");
		const birthYearSpan = document.createElement("span");
		const heightSpan = document.createElement("span");
		const planetSpan = document.createElement("span");

		nameDiv.textContent = "Name: ";
		genderDiv.textContent = "Gender: ";
		skinColorDiv.textContent = "Skin: ";
		birthYearDiv.textContent = "Birth: ";
		heightDiv.textContent = "Height: ";
		planetDiv.textContent = "Planet: ";

		// !!! Запис даних
		nameSpan.innerHTML = this.name;
		genderSpan.innerHTML = this.gender;
		skinColorSpan.innerHTML = this.skinColor;
		birthYearSpan.innerHTML = this.birthYear;
		heightSpan.innerHTML = this.height;
		planetSpan.innerHTML = this.planet;

		nameDiv.append(nameSpan);
		genderDiv.append(genderSpan);
		skinColorDiv.append(skinColorSpan);
		birthYearDiv.append(birthYearSpan);
		heightDiv.append(heightSpan);
		planetDiv.append(planetSpan);

		textBlockDiv.append(nameDiv);
		textBlockDiv.append(genderDiv);
		textBlockDiv.append(skinColorDiv);
		textBlockDiv.append(birthYearDiv);
		textBlockDiv.append(heightDiv);
		textBlockDiv.append(planetDiv);

		// ? Кнопка
		const buttonDiv = document.createElement("div");
		buttonDiv.classList.add("Button");
		buttonDiv.classList.add("ButtonHover");

		const textButtonDiv = document.createElement("div");
		textButtonDiv.classList.add("text_button");
		textButtonDiv.textContent = "Зберегти";

		cardDiv.append(buttonDiv);
		buttonDiv.append(textButtonDiv);

		// TODO Save
		buttonDiv.addEventListener("click", () => {

			if(!sessionStorage.getItem(this.name)) {
				let data = {
					name: this.name,
					gender: this.gender,
					skin: this.skinColor,
					birth: this.birthYear,
					height: this.height,
					planet: this.planet,
				};
				sessionStorage.setItem(this.name, JSON.stringify(data));

				textButtonDiv.textContent = "Збережено";
				textButtonDiv.style.color = "aqua";
				buttonDiv.classList.add("saved");
				buttonDiv.classList.remove("ButtonHover");
			};
			
		});
	};
};

// Запити на сервер
let xhr = new XMLHttpRequest();

let starWarsUrl = "https://swapi.dev/api/people";

xhr.open("GET", starWarsUrl);

xhr.onreadystatechange = function() {
	if(this.readyState == 4 && this.status >= 200 && this.status <= 299) {
		showInfo(JSON.parse(xhr.responseText));
	};
};

xhr.send();

function showInfo(object) {
	console.log(object);

	let arr = object.results;

	arr.forEach((element) => {
		const homeworld = element.homeworld;

		let xhrPlanet = new XMLHttpRequest();

		xhrPlanet.open("GET", homeworld, true);
		xhrPlanet.onload = function() {
			if(this.readyState == 4 && this.status >= 200 && this.status <= 299) {
				let responsed = JSON.parse(xhrPlanet.responseText);

				// ! Створення та виклик класу
				let classCard = new Card(element, responsed);
				classCard.createCard();
			};
		};
		xhrPlanet.send();
	});
};

 






