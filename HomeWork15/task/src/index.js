import React from "react";
import ReactDOM from "react-dom";

const App = function() {
	return (
		<section>
			<div>
				<h3>Week days</h3>
				<ul>
					<li>Monday</li>
					<li>Tuesday</li>
					<li>Wednesday</li>
					<li>Thursday</li>
					<li>Friday</li>
					<li>Saturday</li>
					<li>Sunday</li>
				</ul>
			</div>
			<div>
				<h3>Months</h3>
				<ol>
					<li>Junuary</li>
					<li>February</li>
					<li>March</li>
					<li>April</li>
					<li>May</li>
					<li>June</li>
					<li>July</li>
					<li>August</li>
					<li>September</li>
					<li>October</li>
					<li>November</li>
					<li>December</li>
				</ol>
			</div>
			<Signs></Signs>
		</section>
	)
};

const Signs = function() {
	return (
		<section>
			<h3>Zodiac signs</h3>
		<dl>
			<dt>Aries</dt>
			<dd>March 21 - April 19</dd>

			<dt>Taurus</dt>
			<dd>April 20 - May 20</dd>

			<dt>Gemini</dt>
			<dd>May 21 - June 20</dd>

			<dt>Cancer</dt>
			<dd>June 21 - July 22</dd>

			<dt>Leo</dt>
			<dd>July 23 - August 22</dd>

			<dt>Virgo</dt>
			<dd>August 23 - September 22</dd>

			<dt>Libra</dt>
			<dd>September 23 - October 22</dd>

			<dt>Scorpio</dt>
			<dd>October 23 - November 21</dd>

			<dt>Sagittarius</dt>
			<dd>November 22 - December 21</dd>

			<dt>Capricorn</dt>
			<dd>December 22 - January 19</dd>

			<dt>Aquarius</dt>
			<dd>January 20 - February 18</dd>

			<dt>Pisces</dt>
			<dd>February 19 - March 20</dd>
		</dl>
		</section>
	)
};

ReactDOM.render(<App></App>, document.querySelector("#root"));