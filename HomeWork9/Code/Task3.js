document.write("Введіть номер телефону: ");

const search = document.createElement("input");
search.setAttribute("type", "tel");
search.setAttribute("placeholder", "000-000-00-00");
search.setAttribute("id", "search");

document.body.append(search);

const button = document.createElement("input");
button.setAttribute("type", "button");
button.setAttribute("value", "Зберегти");
button.setAttribute("id", "button");
document.body.append(button);

// Перевірка
const patternNum = new RegExp(/\d{3}-\d{3}-\d{2}-\d{2}/g);
let res;

button.onclick = () => {
	res = patternNum.test(search.value);
	if(res == true) {
		document.body.style.backgroundColor = "green";
		setTimeout(() => {
			document.location = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg"
		}, 2000);
	} else {
		const div = document.createElement("div");
		div.textContent = "Помилка, ви ввели не вірні дані!";
		document.body.prepend(div);
	}
}; 