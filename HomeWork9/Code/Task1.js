const div = document.getElementById("div1");

div.style.margin = "150px";
div.style.width = "50px";
div.style.height = "50px";
div.style.background = "green";

console.log("Відступ: " + div.style.margin); // 1-й спосіб

console.log("Відступ2: " + window.getComputedStyle(div)["margin"]); // 2-й спосіб

console.log("Відступ3: " + div.currentStyle["margin"]); // Вже не працює