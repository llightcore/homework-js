let img = document.getElementById("page");

const images = [
	"https://sundries.com.ua/wp-content/uploads/2021/01/94674-scaled.jpg",
	"https://universetoday.ru/wp-content/uploads/2018/10/Mercury.jpg",
	"https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Mars_Valles_Marineris_EDIT.jpg/274px-Mars_Valles_Marineris_EDIT.jpg",
	"https://tokar.ua/img/upl/2018/11/e4fcbbf4a4d52439596d240a5f9e3bb0.jpg",
	"https://gloss.ua/img/article/1387/25_tn-v1661967275.jpg"
];

let count = 0;
let opacit = 0;

setInterval(() => {
	if(count >= images.length) count = 0;

	let timer = setInterval(() => {
		img.style.opacity = opacit;
		opacit = opacit + 0.1;
		if(opacit > 1) {
			clearInterval(timer);
			opacit = 0;
		}
	}, 100);
	
	img.setAttribute("src", images[count]);
	img.style.width = "50vh";
	img.style.height = "400px";
	img.style.opacity = "0";

	count++;

}, 3000);
