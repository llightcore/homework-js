const getId = id => document.getElementById(id);
const btnStart = getId("btnStart");
const btnStop = getId("btnStop");
const btnReset = getId("btnReset");

const hour = getId("hour");
const minut = getId("minut");
const second = getId("second");

let colorTab = document.querySelector(".colorTab");

let countSec = 0,
countMin = 0,
countHour = 0;
let timer;

btnStart.onclick = () => {
	colorTab.classList.remove("red", "grey");
	colorTab.classList.add("green");

	function count() {
		(countSec < 10) ? second.innerHTML = "0" + countSec : second.innerHTML = countSec;
	
		if(countSec > 59) {
			countSec = 0;
			second.innerHTML = "0" + countSec;

			(countMin < 9) ? minut.innerHTML = "0" + (countMin += 1) : minut.innerHTML = countMin += 1;
		};

		if(countMin > 59) {
			countMin = 0;
			minut.innerHTML = "0" + countMin;

			(countHour < 9) ? hour.innerHTML = "0" + (countHour += 1) : hour.innerHTML = countHour += 1;
		};

		countSec++;
	};

	timer = setInterval(count, 1000);
	
};

btnStop.onclick = () => {
	colorTab.classList.remove("green", "grey");
	colorTab.classList.add("red");

	clearInterval(timer);
};

btnReset.onclick = () => {
	colorTab.classList.remove("green", "red");
	colorTab.classList.add("grey");

	countSec = 0;
	second.innerHTML = "0" + countSec;
	countMin = 0;
	minut.innerHTML = "0" + countMin;
	countHour = 0;
	hour.innerHTML = "0" + countHour;
	clearInterval(timer);

};